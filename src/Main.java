import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Person> allCandidates = new ArrayList<>(Person.generateCandidates(10));

        List<Astronaut> list = allCandidates.stream()
                .limit(8)                              // первых 8 кандидатов
                .sorted((c1, c2) -> c2.age - c1.age)           // сортировка по убыванию возраста
                .filter(c -> c.age >= 20)                      // фильтр на слишком молодых
                .filter(c -> c.age <= 45)                      // фильтр на слишком старых
                .peek(System.out::println)                     // вывод кандидатов в консоль
                .map(c -> new Astronaut(c.name, c.age))        // преобразование в объект Космонавт
                .sorted((c1, c2) -> c1.name.compareTo(c2.name)) // сортировка по алфавиту
                .toList();

        System.out.println(list);
    }
}