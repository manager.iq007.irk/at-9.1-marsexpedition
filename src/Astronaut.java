public class Astronaut {
    String name;
    int age;
    String departPlanet;

    Astronaut(String name, int age) { //Конструктор
        this.name = name;
        this.age = age;
    }

    public String toString() { //Вывод информации о кандидате
        return "\n Обнаружен кандидат в космонавты! " +
                "\n Имя астронавта: " + this.name +
                "\n Возраст астронавта: " + this.age;
    }
}
