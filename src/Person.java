import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Person {
    long id;
    String name;
    int age;

    static String[] namesArray = {"Gerald", "Henry", "William", "Emma", "Ariana", "Jessica", "Monica", "Antonio", "Johnny", "Daniel", "Jack", "Rupert"};

    private static int counter = 0;

    public static int getCount() {
        return counter;
    }
    public static void increment() {
        counter++;
    }

    Person(String name, int age) { //Конструктор
        this.name = name;
        this.id = getCount();
        increment();
        this.age = age;
    }


    public static List<Person> generateCandidates(int amount) {
        // Метод генерирует объекты Person в количестве amount. Генерируются автоматически имя, возраст и айди
        Random random = new Random();
        Person[] generatedPersons = new Person[amount];
        for (int i = 0; i < generatedPersons.length; i++) {
            generatedPersons[i] = new Person(namesArray[new Random().nextInt(namesArray.length)], random.nextInt(16, 65));
        }
        return Arrays.asList(generatedPersons);
    }



    public String toString() { //Вывод информации о кандидате
        return "Идентификатор (ID): " + this.id
                + "\n Имя: " + this.name
                + "\n Возраст: " + this.age;
    }

}
